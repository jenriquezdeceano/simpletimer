const { BrowserWindow, ipcMain } = require("electron");
const TrayIcon = require('./tray/TrayIcon');



let trayIcon = null;
function createWindow() {
  const win = new BrowserWindow({
    width: 300,
    height: 700,
    frame:false,
    webPreferences: {
      nodeIntegration: true
    }
  });
  trayIcon = new TrayIcon(win);
  win.loadFile("app/index.html");
  win.hide();
}


ipcMain.on('timer', function(event, arg) {
 trayIcon.updateTitle("\u001b[37;1m"+arg);
});

module.exports = { createWindow };
