const path = require('path');
const { BrowserWindow, Tray, ipcMain } = require('electron');
const Positioner = require('electron-positioner');


class TrayIcon {
  constructor(trayWindow) {

    let iconPath = path.join(__dirname, '/icon-22.png')
    this.trayIcon = new Tray(iconPath);
    this.trayIcon.setToolTip('Time Tracker'); // This tooltip will show up, when user hovers over our tray-icon.
    this.trayIcon.on('click', (e, bounds) => {
      if ( trayWindow.isVisible() ) {
        trayWindow.hide();
      } else {
        let positioner = new Positioner(trayWindow);
        positioner.move('trayCenter', bounds)
        trayWindow.show();
      }
    });
  }
  updateTitle(arg) {
    this.trayIcon.setTitle(arg);
  }

}



module.exports = TrayIcon;
