const fs = require('fs');
const { ipcRenderer, ipcMain } = require("electron");
const startBtn = document.getElementById("startBtn");

var minutesLabel = document.getElementById("minutes");
var secondsLabel = document.getElementById("seconds");
var totalSeconds = 0;

   
  function saveTimer(){
    let myfile='myfile.txt';
    let elapsed = minutesLabel.innerHTML +":"+ secondsLabel.innerHTML;
    try { 
      fs.writeFileSync(myfile,elapsed , 'utf-8'); 
    }catch(e) { 
      alert('Failed to save the file !'); 
    }

}
let intervalId;
function start(){
  if(startBtn.innerText==="start"){
    startBtn.innerText="stop";
    intervalId = setInterval(()=>{
      ++totalSeconds;
      secondsLabel.innerHTML = pad(totalSeconds % 60);
      minutesLabel.innerHTML = pad(parseInt(totalSeconds / 60));
      ipcRenderer.send("timer",minutesLabel.innerHTML +":"+secondsLabel.innerHTML);
    }, 1000);
  } else if(startBtn.innerText==="stop"){
    startBtn.innerText="start";
    stop();
  }
}

function stop(){
  clearInterval(intervalId);
  saveTimer();
}

function setTime() {
  ++totalSeconds;
  secondsLabel.innerHTML = pad(totalSeconds % 60);
  minutesLabel.innerHTML = pad(parseInt(totalSeconds / 60));
}

function pad(val) {
  var valString = val + "";
  if (valString.length < 2) {
    return "0" + valString;
  } else {
    return valString;
  }
}


